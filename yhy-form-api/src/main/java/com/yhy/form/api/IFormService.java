package com.yhy.form.api;

import com.yhy.common.api.IBaseApiService;
import com.yhy.form.dto.FormSetApiDTO;

import java.util.List;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-8 上午11:00 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public interface IFormService extends IBaseApiService {

    String SERVICE_BEAN = "apiFormService";

    List<FormSetApiDTO> getFormSetByCode(Set<String> codes);

}
