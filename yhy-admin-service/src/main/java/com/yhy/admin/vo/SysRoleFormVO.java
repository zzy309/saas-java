package com.yhy.admin.vo;

import com.yhy.common.dto.BaseEntity;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-20 上午9:26 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class SysRoleFormVO extends BaseEntity {
	
    /**
     * 角色 ID db_column: ROLE_ID 
     */	
	private String roleId;
    /**
     * 表单系统编号 db_column: FORM_CODE 
     */	
	private String formCode;
    /**
     * 表单名称 db_column: FORM_NAME 
     */	
	private String formName;

	public SysRoleFormVO(){
	}

	public SysRoleFormVO( String id ){
		this.id = id;
	}

	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getRoleId() {
		return this.roleId;
	}
	
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getFormCode() {
		return this.formCode;
	}
	
	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}
	public String getFormName() {
		return this.formName;
	}
	
	public void setFormName(String formName) {
		this.formName = formName;
	}

}

