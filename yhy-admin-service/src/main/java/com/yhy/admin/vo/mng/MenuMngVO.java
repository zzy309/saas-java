package com.yhy.admin.vo.mng;

import com.yhy.common.dto.BaseMngVO;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-11-13 下午4:27 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-1 下午5:42 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
public class MenuMngVO extends BaseMngVO {


    private String menuName;
    private String menuType;
    private String parentId;
    private String componentUrl;
    private String pathUrl;
    private Integer sort;
    private String icon;
    private String isAdmin;

    private String moduleBusClass;
    private String isVisable;
    private String isWorkflow;
    private String privCode;

    private List<MenuMngVO> children;

    public MenuMngVO() {
    }

    public String getPrivCode() {
        return privCode;
    }

    public void setPrivCode(String privCode) {
        this.privCode = privCode;
    }

    public String getModuleBusClass() {
        return moduleBusClass;
    }

    public void setModuleBusClass(String moduleBusClass) {
        this.moduleBusClass = moduleBusClass;
    }

    public String getIsVisable() {
        return isVisable;
    }

    public void setIsVisable(String isVisable) {
        this.isVisable = isVisable;
    }

    public String getIsWorkflow() {
        return isWorkflow;
    }

    public void setIsWorkflow(String isWorkflow) {
        this.isWorkflow = isWorkflow;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getLabel() {
        return menuName;
    }

    public List<MenuMngVO> getChildren() {
        return children;
    }

    public void setChildren(List<MenuMngVO> children) {
        this.children = children;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getComponentUrl() {
        return componentUrl;
    }

    public void setComponentUrl(String componentUrl) {
        this.componentUrl = componentUrl;
    }

    public String getPathUrl() {
        return pathUrl;
    }

    public void setPathUrl(String pathUrl) {
        this.pathUrl = pathUrl;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
