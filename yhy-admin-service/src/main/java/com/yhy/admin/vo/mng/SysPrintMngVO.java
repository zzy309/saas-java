package com.yhy.admin.vo.mng;

import com.yhy.common.dto.BaseMngVO;
/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-31 下午5:14 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

public class SysPrintMngVO extends BaseMngVO {

    private String reportCode;
    /**
     * 报表名称 db_column: REPORT_NAME
     */
    private String reportName;
    /**
     * 附档ID对应 t_sys_attachment.id db_column: ATTACHMENT_ID
     */
    private String attachmentId;
    /**
     * 报表sql db_column: ATTACHMENT_PATH
     */
    private String attachmentPath;
    /**
     * 排序 db_column: REPORT_SORT
     */
    private Integer reportSort;

    private String printBean;


    public SysPrintMngVO() {
    }

    public String getReportCode() {
        return reportCode;
    }

    public void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

    public Integer getReportSort() {
        return reportSort;
    }

    public void setReportSort(Integer reportSort) {
        this.reportSort = reportSort;
    }

    public String getPrintBean() {
        return printBean;
    }

    public void setPrintBean(String printBean) {
        this.printBean = printBean;
    }
}
