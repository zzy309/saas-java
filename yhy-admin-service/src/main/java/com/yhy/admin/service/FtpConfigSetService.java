package com.yhy.admin.service;

import com.yhy.admin.dao.FtpConfigSetDao;
import com.yhy.admin.vo.FtpConfigSet;
import com.yhy.common.config.CustomJasyptStringEncryptor;
import com.yhy.common.config.EnvConfig;
import com.yhy.common.service.BaseMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-19 上午11:01 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class FtpConfigSetService extends BaseMainService<FtpConfigSet> {

    private static volatile Map<String,FtpConfigSet> ftpConfigSetMap = new ConcurrentHashMap<>();
    @Autowired
    private FtpConfigSetDao ftpConfigSetDao;

    @Override
    protected FtpConfigSetDao getDao() {
        return ftpConfigSetDao;
    }


    public FtpConfigSet getSystemFtpConfig() {
        return getFtpConfigByCode("SYSTEM");
    }

    public void removeFtpConfigByCode(String code) {
        ftpConfigSetMap.remove(code);
    }

    /*
    *  获取默认FTP配置
     */
    public FtpConfigSet getDefaultFtpConfig(String cpyCode) {
        FtpConfigSet configSet = ftpConfigSetDao.getDefaultFtpConfig(cpyCode);
        if(configSet != null) {
            String password = new CustomJasyptStringEncryptor().decrypt(configSet.getPassword());
            configSet.setPassword(password);
        }
        return configSet;
    }

    public List<FtpConfigSet> getAllFtpConfig(String cpyCode) {
        return ftpConfigSetDao.getAllFtpConfig(cpyCode);
    }

    /*
     *  根据ID获取FTP配置
     */
    public FtpConfigSet getFtpConfigByCode(String code) {
        FtpConfigSet ftpConfigSet = ftpConfigSetMap.get(ftpConfigSetMap);
        if(ftpConfigSet != null) {
            return ftpConfigSet;
        }

        synchronized (FtpConfigSetService.class) {
            ftpConfigSet = ftpConfigSetMap.get(ftpConfigSetMap);
            if(ftpConfigSet != null) {
                return ftpConfigSet;
            }
            if("SYSTEM".equals(code)) {
                ftpConfigSet = new FtpConfigSet();
                ftpConfigSet.setIp(EnvConfig.getValueByPropertyName("ftp.ip"));
                ftpConfigSet.setPort(Integer.parseInt(EnvConfig.getValueByPropertyName("ftp.port")));
                ftpConfigSet.setUser(EnvConfig.getValueByPropertyName("ftp.user"));
                String password = new CustomJasyptStringEncryptor().decrypt(EnvConfig.getValueByPropertyName("ftp.password"));
                ftpConfigSet.setPassword(password);
                ftpConfigSetMap.put(code,ftpConfigSet);
            } else {
                List<FtpConfigSet> configSets = ftpConfigSetDao.findAll();
                for (FtpConfigSet configSet : configSets) {
                    String password = new CustomJasyptStringEncryptor().decrypt(configSet.getPassword());
                    configSet.setPassword(password);
                    ftpConfigSetMap.put(configSet.getId(),configSet);
                }
            }
        }
        return ftpConfigSetMap.get(code);
    }

}
