package com.yhy.admin.dto;

import com.yhy.admin.vo.SysJobVO;
import com.yhy.admin.vo.mng.SysJobMngVO;
import com.yhy.common.dto.BaseMngDTO;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-25 下午1:56 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class SysJobDTO extends BaseMngDTO<SysJobMngVO> {

    @Override
    public SysJobVO getBusMain() {
        return new SysJobVO();
    }

}
