package com.yhy.admin.dao;

import com.yhy.admin.dto.CpyDTO;
import com.yhy.admin.dto.SysPrintDTO;
import com.yhy.admin.vo.mng.CpyMngVO;
import com.yhy.admin.vo.mng.SysPrintMngVO;
import com.yhy.common.dao.BaseComplexMngDao;
import com.yhy.common.dao.BaseMngDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-31 下午5:12 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Mapper
@Component(value = "sysPrintMngDao")
public interface SysPrintMngDao extends BaseComplexMngDao<SysPrintDTO,SysPrintMngVO> {

    List<SysPrintMngVO> findValidDataByReportCode(@Param("sysOwnerCpy") String sysOwnerCpy, @Param("reportCode")String reportCode);


}
