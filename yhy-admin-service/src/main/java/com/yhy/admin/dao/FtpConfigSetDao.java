package com.yhy.admin.dao;

import com.yhy.admin.vo.FtpConfigSet;
import com.yhy.common.dao.BaseMainDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-19 上午11:01 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Mapper
@Component(value = "ftpConfigSetDao")
public interface FtpConfigSetDao extends BaseMainDao<FtpConfigSet> {

    FtpConfigSet getDefaultFtpConfig(@Param("cpyCode") String cpyCode);

    List<FtpConfigSet> getAllFtpConfig(@Param("cpyCode") String cpyCode);

}
