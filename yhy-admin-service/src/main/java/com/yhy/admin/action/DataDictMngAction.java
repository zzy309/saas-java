package com.yhy.admin.action;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.yhy.admin.dto.DataDictDTO;
import com.yhy.admin.service.mng.DataDictMngService;
import com.yhy.admin.vo.mng.DataDictMngVO;
import com.yhy.common.action.BaseMngAction;
import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.dto.ReturnCode;
import com.yhy.common.intercept.PreCheckPermission;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.SysUser;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-6-26 下午3:01 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@RestController
@RequestMapping(value="/admin-service/api/dataDictMng", produces="application/json;charset=UTF-8")
public class DataDictMngAction extends BaseMngAction<DataDictDTO> {

    @Autowired
    private DataDictMngService dataDictMngService;

    @Override
    protected DataDictMngService getBaseService() {
        return dataDictMngService;
    }

    /*
     * 获取数据字典信息
     */
    @RequestMapping(value="/getDictByType",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取数据字典信息", notes="获取数据字典信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dictType", value = "字典类别", required = true, dataType = "String")
    })
    public AppReturnMsg getDictByType(HttpServletRequest request, @RequestParam("dictType") String dictType) {
        String lanCode = YhyUtils.getSysUser().getLanCode();
        Map<String,List<DataDictMngVO>> rtnVO = Maps.newHashMap();
        for (String tmpType : dictType.split(",")) {
            rtnVO.put(tmpType,getBaseService().getDictByType(tmpType,lanCode));
        }
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"",rtnVO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN"})
    @RequestMapping(value="/toAdd",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="新增操作", notes="新增操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = DataDictDTO.class)
    })
    public AppReturnMsg toAdd(HttpServletRequest request, HttpServletResponse response, @RequestBody DataDictDTO baseDTO) {
        return super.toAddCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN"})
    @RequestMapping(value="/toCopy",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="复制操作", notes="复制操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = DataDictDTO.class)
    })
    public AppReturnMsg toCopy(HttpServletRequest request, HttpServletResponse response, @RequestBody DataDictDTO baseDTO) {
        return super.toCopyCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","DATADICT_ALL","DATADICT_CREATE","DATADICT_CHANGE"})
    @RequestMapping(value="/doSave",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "DataDictDTO")
    })
    public AppReturnMsg doSave(HttpServletRequest request, HttpServletResponse response, @RequestBody DataDictDTO baseDTO) {
        return super.doSaveCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","DATADICT_ALL","DATADICT_SELECT"})
    @RequestMapping(value="/loadData",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="查询数据", notes="查询操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "DataDictDTO")
    })
    public AppReturnMsg loadData(HttpServletRequest request, HttpServletResponse response, @RequestBody DataDictDTO baseDTO) {
        SysUser sysUser = YhyUtils.getSysUser();
        sysUser.setCpyCode(null);
        baseDTO.setSysUser(sysUser);
        return super.loadDataCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","DATADICT_ALL","DATADICT_DELETE"})
    @RequestMapping(value="/doDelete",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除数组", required = true, dataType = "BaseDTO")
    })
    public AppReturnMsg doDelete(HttpServletRequest request, HttpServletResponse response, @RequestBody DataDictDTO baseDTO) {
        return super.doDeleteCommon(request, response, baseDTO);
    }
}
