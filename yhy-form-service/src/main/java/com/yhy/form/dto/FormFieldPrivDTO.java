package com.yhy.form.dto;

import com.google.common.collect.Lists;
import com.yhy.common.dto.BaseEntity;
import com.yhy.common.dto.BaseMngDTO;
import com.yhy.form.vo.FormSetContentVO;
import com.yhy.form.vo.FormSetFieldPrivVO;
import com.yhy.form.vo.FormSetFieldVO;
import com.yhy.form.vo.FormSetMainVO;
import com.yhy.form.vo.mng.FormSetMngVO;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-2-26 下午3:38 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class FormFieldPrivDTO extends BaseEntity {

    private FormSetFieldVO fieldVO;
    private FormSetFieldPrivVO visibleFieldPrivVO;
    private FormSetFieldPrivVO editFieldPrivVO;

    public FormSetFieldVO getFieldVO() {
        return fieldVO;
    }

    public void setFieldVO(FormSetFieldVO fieldVO) {
        this.fieldVO = fieldVO;
    }

    public FormSetFieldPrivVO getVisibleFieldPrivVO() {
        return visibleFieldPrivVO;
    }

    public void setVisibleFieldPrivVO(FormSetFieldPrivVO visibleFieldPrivVO) {
        this.visibleFieldPrivVO = visibleFieldPrivVO;
    }

    public FormSetFieldPrivVO getEditFieldPrivVO() {
        return editFieldPrivVO;
    }

    public void setEditFieldPrivVO(FormSetFieldPrivVO editFieldPrivVO) {
        this.editFieldPrivVO = editFieldPrivVO;
    }
}
