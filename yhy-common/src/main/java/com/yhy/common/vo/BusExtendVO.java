package com.yhy.common.vo;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.yhy.common.dto.BaseEntity;

import java.math.BigDecimal;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-22 上午10:33 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class BusExtendVO extends BaseEntity {
	
    /**
     * 主表ID db_column: MAIN_ID 
     */	
	private String mainId;
    /**
     * formApplyId db_column: FORM_APPLY_ID 
     */	
	private String formApplyId;
	private String sourceFormApplyId; //来源ID
    /**
     * 对应扩展主表ID db_column: FORM_ID 
     */	
	private String formId;
    /**
     * formCode db_column: FORM_CODE 
     */	
	private String formCode;
    /**
     * busModule db_column: BUS_MODULE 
     */	
	private String busModule;
    /**
     * busClass db_column: BUS_CLASS 
     */	
	private String busClass;
    /**
     * attribute3 db_column: ATTRIBUTE3 
     */	
	private String attribute3;
    /**
     * attribute4 db_column: ATTRIBUTE4 
     */	
	private String attribute4;
    /**
     * attribute5 db_column: ATTRIBUTE5 
     */	
	private String attribute5;
    /**
     * attribute6 db_column: ATTRIBUTE6 
     */	
	private String attribute6;
    /**
     * 更新版本号 db_column: ATTRIBUTE7 
     */	
	private BigDecimal attribute7;
    /**
     * attribute8 db_column: ATTRIBUTE8 
     */	
	private BigDecimal attribute8;
    /**
     * attribute9 db_column: ATTRIBUTE9 
     */	
	private java.util.Date attribute9;
    /**
     * attribute10 db_column: ATTRIBUTE10 
     */	
	private java.util.Date attribute10;

	public BusExtendVO(){
	}

	public String getSourceFormApplyId() {
		return sourceFormApplyId;
	}

	public void setSourceFormApplyId(String sourceFormApplyId) {
		this.sourceFormApplyId = sourceFormApplyId;
	}

	public BusExtendVO(String id ){
		this.id = id;
	}
	public String getMainId() {
		return this.mainId;
	}
	
	public void setMainId(String mainId) {
		this.mainId = mainId;
	}
	public String getFormApplyId() {
		return this.formApplyId;
	}
	
	public void setFormApplyId(String formApplyId) {
		this.formApplyId = formApplyId;
	}
	public String getFormId() {
		return this.formId;
	}
	
	public void setFormId(String formId) {
		this.formId = formId;
	}
	public String getFormCode() {
		return this.formCode;
	}
	
	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}
	public String getBusModule() {
		return this.busModule;
	}
	
	public void setBusModule(String busModule) {
		this.busModule = busModule;
	}
	public String getBusClass() {
		return this.busClass;
	}
	
	public void setBusClass(String busClass) {
		this.busClass = busClass;
	}
	public String getAttribute3() {
		return this.attribute3;
	}
	
	public void setAttribute3(String attribute3) {
		this.attribute3 = attribute3;
	}
	public String getAttribute4() {
		return this.attribute4;
	}
	
	public void setAttribute4(String attribute4) {
		this.attribute4 = attribute4;
	}
	public String getAttribute5() {
		return this.attribute5;
	}
	
	public void setAttribute5(String attribute5) {
		this.attribute5 = attribute5;
	}
	public String getAttribute6() {
		return this.attribute6;
	}
	
	public void setAttribute6(String attribute6) {
		this.attribute6 = attribute6;
	}
	public BigDecimal getAttribute7() {
		return this.attribute7;
	}
	
	public void setAttribute7(BigDecimal attribute7) {
		this.attribute7 = attribute7;
	}
	public BigDecimal getAttribute8() {
		return this.attribute8;
	}
	
	public void setAttribute8(BigDecimal attribute8) {
		this.attribute8 = attribute8;
	}
	public java.util.Date getAttribute9() {
		return this.attribute9;
	}
	
	public void setAttribute9(java.util.Date attribute9) {
		this.attribute9 = attribute9;
	}
	public java.util.Date getAttribute10() {
		return this.attribute10;
	}
	
	public void setAttribute10(java.util.Date attribute10) {
		this.attribute10 = attribute10;
	}
	
}

