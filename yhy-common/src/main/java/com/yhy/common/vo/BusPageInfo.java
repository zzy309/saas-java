package com.yhy.common.vo;


/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-12-13 上午9:13 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
public class BusPageInfo {

	protected Boolean isEditable = true;

	public BusPageInfo() {
	}

	public Boolean getEditable() {
		return isEditable;
	}

	public void setEditable(Boolean editable) {
		isEditable = editable;
	}
}
