package com.yhy.common.utils;

/**
 * <br>
 * <b>功能：</b>事务执行下文接口<br>
 */
public interface ITransationExecutor<T, E> {

	T execute(E param);
}
