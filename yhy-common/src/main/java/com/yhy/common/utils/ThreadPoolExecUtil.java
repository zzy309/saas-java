package com.yhy.common.utils;

import com.yhy.common.config.CustomThreadFactory;
import com.yhy.common.config.ThreadTaskProperties;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：线程池工作类</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-12-12 上午10:33 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
public class ThreadPoolExecUtil {
    public static ThreadPoolExecutor getThreadPoll(String threadName){
        ThreadTaskProperties properties = SpringContextHolder.getBean(ThreadTaskProperties.class);
        return new ThreadPoolExecutor(
                properties.getCorePoolSize(),
                properties.getMaxPoolSize(),
                properties.getKeepAliveSeconds(),
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(properties.getQueueSize()),
                new CustomThreadFactory(threadName)
        );
    }

}
