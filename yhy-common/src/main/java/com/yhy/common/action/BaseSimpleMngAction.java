package com.yhy.common.action;

import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.dto.BaseMngDTO;
import com.yhy.common.dto.BaseMngVO;
import com.yhy.common.dto.ReturnCode;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.service.BaseComplexMngService;
import com.yhy.common.service.BaseSimpleMngService;
import com.yhy.common.utils.BusCenterHelper;
import com.yhy.common.utils.ITransationExecutor;
import com.yhy.common.utils.YhyUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-25 下午11:07 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

public abstract class BaseSimpleMngAction<T extends BaseMngDTO> extends BaseMngAction<T> {

	protected abstract BaseSimpleMngService getBaseService();

    protected AppReturnMsg toChangeCommon(HttpServletRequest request, HttpServletResponse response, T baseDTO) {
        getBaseService().toChangeData(baseDTO);
        AppReturnMsg returnMsg = new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"",baseDTO,null);
        afterPublicViewInfo(baseDTO,returnMsg);
        return returnMsg;
    }

	protected AppReturnMsg doDestoryCommon(HttpServletRequest request, HttpServletResponse response, T baseDTO) {
		if (CollectionUtils.isEmpty(baseDTO.getIdList())) {
			throw new BusinessException("业务ID不能为空.");
		}

		AppReturnMsg appReturnMsg = multTransactionExecute(request,response, baseDTO, new ITransationExecutor<Boolean,String>() {
			@Override
			public Boolean execute(String busId) {
				BaseMngVO busMainData = getBaseService().findByBusId(busId);
				getBaseService().doSingleDestory(baseDTO, busMainData);
				return true;
			}
		});
		return appReturnMsg;
	}

}
