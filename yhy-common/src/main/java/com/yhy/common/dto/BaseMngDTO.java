package com.yhy.common.dto;

import com.yhy.common.vo.*;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyuan<br>
 *  *  * <b>日期：</b> 19-6-26 下午3:45 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

public abstract class BaseMngDTO<E extends BaseMngVO> extends BaseDTO<E> {

    private OrderVO orderBean;

    private BusPageInfo busPageInfo;

    private E busMainData;

    private BusCommonState busCommonState;

    private BusExtendVO busExtend;

    private List<SysAttachment> attachmentList;

    private String funProcess;
    //表单保存的直接提交
    private Boolean isFormSubmit = false;
    //列表选择的提交
    private Boolean isListSubmit = false;

    private String operateType;
    private SysPrintMainVO printMain;
    private String reportType;
    // 是否从历史表中取记录
    private Boolean isHistoryTable;

    public BaseMngDTO() {

    }

    public Boolean getHistoryTable() {
        return isHistoryTable;
    }

    public void setHistoryTable(Boolean historyTable) {
        isHistoryTable = historyTable;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public SysPrintMainVO getPrintMain() {
        return printMain;
    }

    public void setPrintMain(SysPrintMainVO printMain) {
        this.printMain = printMain;
    }

    public BusPageInfo getBusPageInfo() {
        return busPageInfo;
    }

    public void setBusPageInfo(BusPageInfo busPageInfo) {
        this.busPageInfo = busPageInfo;
    }

    public Boolean getFormSubmit() {
        return isFormSubmit;
    }

    public void setFormSubmit(Boolean formSubmit) {
        isFormSubmit = formSubmit;
    }

    public Boolean getListSubmit() {
        return isListSubmit;
    }

    public void setListSubmit(Boolean listSubmit) {
        isListSubmit = listSubmit;
    }

    public List<SysAttachment> getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(List<SysAttachment> attachmentList) {
        this.attachmentList = attachmentList;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public String getFunProcess() {
        return funProcess;
    }

    public void setFunProcess(String funProcess) {
        this.funProcess = funProcess;
    }

    public abstract BaseMainEntity getBusMain();

    public E getBusMainData() {
        return busMainData;
    }

    public void setBusMainData(E busMainData) {
        this.busMainData = busMainData;
    }

    public OrderVO getOrderBean() {
        return orderBean;
    }

    public void setOrderBean(OrderVO orderBean) {
        this.orderBean = orderBean;
    }

    public BusCommonState getBusCommonState() {
        if(busCommonState == null) {
            busCommonState = new BusCommonState();
        }
        return busCommonState;
    }

    public BusExtendVO getBusExtend() {
        return busExtend;
    }

    public void setBusExtend(BusExtendVO busExtend) {
        this.busExtend = busExtend;
    }

    public void setBusCommonState(BusCommonState busCommonState) {
        this.busCommonState = busCommonState;
    }


}
