package com.yhy.common.context;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-11-28 上午10:15 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

import java.util.WeakHashMap;

public class ThreadContextInfo  extends WeakHashMap<String, Object> {

    public Object getParameter(String name) {
        return this.get(name);
    }

    public void setParameter(String name, Object value) {
        this.put(name, value);
    }

    public Object removeParameter(String name) {
        return this.remove(name);
    }

}
