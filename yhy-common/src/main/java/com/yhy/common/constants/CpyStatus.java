package com.yhy.common.constants;

import com.yhy.common.exception.BusinessException;

import java.util.HashMap;
import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-6-28 上午9:49 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
public enum CpyStatus {

	// 发送方
	ONLINE("ONLINE","在线"),

	// 接收方
	OFFLINE("OFFLINE","离线");

	private final String val;
	private final String labCode;

	private static Map<String, CpyStatus> cpyStatusMap;

	CpyStatus(String val, String labCode) {
		this.val = val;
		this.labCode = labCode;
	}

	public String getVal() {
		return val;
	}

	public String getLabel() {
		return labCode;
	}

	public static CpyStatus getInstByVal(String val) {
		if (cpyStatusMap == null) {
			synchronized (CpyStatus.class) {
				if (cpyStatusMap == null) {
					cpyStatusMap = new HashMap<String, CpyStatus>();
					for (CpyStatus busState : CpyStatus.values()) {
						cpyStatusMap.put(busState.getVal(), busState);
					}
				}
			}
		}
		String key = val;
		if (!cpyStatusMap.containsKey(key)) {
			throw new BusinessException("状态值" + val + "对应的CpyStatus枚举值不存在。");
		}
		return cpyStatusMap.get(key);
	}

}
